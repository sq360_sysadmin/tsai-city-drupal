<?php

/**
 * @file
 * settings.pantheon.test.php
 */

$config['config_split.config_split.dev']['status'] = FALSE;

// Environment indicator.
$settings['simple_environment_indicator'] = '#ffd000 TEST';

// imgIX Settings.
$config['imgix']['url'] = 'https://tsai-city.imgix.net/';

// imagemagick path
$config['imagemagick.settings']['path_to_binaries'] = '/usr/bin/';
