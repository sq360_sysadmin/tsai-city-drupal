<?php

/**
 * @file
 * settings.pantheon.multidev.php
 */

$config['config_split.config_split.migrate']['status'] = TRUE;

$settings['simple_environment_indicator'] = '#ff8600 multi-DEV';

// Disable CSS and JS aggregation.
$config['system.performance']['css']['preprocess'] = FALSE;
$config['system.performance']['js']['preprocess'] = FALSE;

// Disable render cache.
$settings['cache']['bins']['render'] = 'cache.backend.null';
$settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';

// imgIX Settings.
$config['imgix']['url'] = 'https://tsai-city.imgix.net/';

// imagemagick path
$config['imagemagick.settings']['path_to_binaries'] = '/usr/bin/';
