<?php

// 301 Redirect from /programs-research to /programs
// Check if Drupal or WordPress is running via command line
if ((strpos($_SERVER['REQUEST_URI'], '/blog/') !== false) && (php_sapi_name() != "cli")) {
  $newRequestURI = str_replace('/blog/', '/stories/', $_SERVER['REQUEST_URI']);

  header('HTTP/1.0 301 Moved Permanently');
  header('Location: ' . $newRequestURI);

  // Name transaction "redirect" in New Relic for improved reporting (optional).
  if (extension_loaded('newrelic')) {
    newrelic_name_transaction("redirect");
  }
  exit();
}
