<?php

/**
 * @file
 * settings.pantheon.live.php
 */

$config['config_split.config_split.dev']['status'] = FALSE;

// Environment indicator.
$settings['simple_environment_indicator'] = '#007d1f LIVE';

// imgIX Settings.
$config['imgix']['url'] = 'https://tsai-city-live.imgix.net/';

// imagemagick path
$config['imagemagick.settings']['path_to_binaries'] = '/usr/bin/';
