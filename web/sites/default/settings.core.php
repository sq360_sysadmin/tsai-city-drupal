<?php

/**
 * @file
 * settings.core.php
 *
 * This file contains all Drupal specific configuration settings.
 *
 * NOTICE: THE SITE WILL NOT RUN WITHOUT THESE SETTINGS.
 */

// Database settings.
$databases = [];

// Salt for one-time login links, cancel links, form tokens, etc.
// Run `drush php-eval 'echo \Drupal\Component\Utility\Crypt::randomBytesBase64(55) . "\n";'` to generate.
$settings['hash_salt'] = 'dLbEewboBNXBZUSyrscA9NQHasEZfim4qWPw7zEeFQy4hi4n7ReMgrZkyZtOsfSn-qPEm4FjZw';

// Access control for update.php script.
$settings['update_free_access'] = FALSE;

// The default number of entities to update in a batch process.
$settings['entity_update_batch_size'] = 50;

// Skip file system permissions hardening.
// This should be removed or FALSE for Pantheon.
$settings['skip_permissions_hardening'] = TRUE;

// Install the 'standard' profile settings.php file isn't modified.
$settings['install_profile'] = 'standard';
