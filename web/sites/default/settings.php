<?php

/**
 * @file
 * settings.php
 *
 * Drupal configuration file.
 *
 * The require_once order of the *.php files is important.
 * The "settings.core.php" most come first regardless of server environment.
 *
 * Pantheon server:
 * Make sure the "settings.pantheon.php" is before the "settings.global.php".
 * Certain configurations are overridden because their defaults from Pantheon
 * are not ideal.
 *
 * All other servers:
 * You can remove all code that references the server(s) you will not be using.
 */

require_once __DIR__ . '/settings.core.php';

// Pantheon specific settings file.
$settings_pantheon = __DIR__ . '/settings.pantheon.php';

if (file_exists($settings_pantheon)) {
  // The settings.pantheon.php file makes some changes that affect all
  // environments that this site exists in. Always include this file, even in
  // a local development environment, to ensure that the site settings remain
  // consistent.
  include $settings_pantheon;
}

require_once __DIR__ . '/settings.global.php';

// Pantheon server.
if (isset($_ENV['PANTHEON_ENVIRONMENT'])) {
  $env_settings_file = __DIR__ . '/settings.pantheon.' . $_ENV['PANTHEON_ENVIRONMENT'] . '.php';

  if (file_exists($env_settings_file)) {
    require_once $env_settings_file;
  }
  elseif (file_exists(__DIR__ . '/settings.pantheon.multidev.php')) {
    require_once __DIR__ . '/settings.pantheon.multidev.php';
  }
}

// Local server.
$settings_local = __DIR__ . '/settings.local.php';

if (file_exists($settings_local)) {
  require_once $settings_local;
}
