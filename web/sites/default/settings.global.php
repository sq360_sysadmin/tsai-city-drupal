<?php

/**
 * @file
 * settings.global.php
 *
 * This file contains all global configuration settings which are used
 * across all domains.
 *
 * Any configuration that is global across all domains that is not "CORE" to
 * Drupal should be put in this file.
 */

// Location of the site configuration files.
$settings['config_sync_directory'] = dirname(DRUPAL_ROOT) . '/config/sync';

// Fast 404 pages.
$config['system.performance']['fast_404']['exclude_paths'] = '/\/(?:styles)|(?:system\/files)\//';
$config['system.performance']['fast_404']['paths'] = '/\.(?:txt|png|gif|jpe?g|css|js|ico|swf|flv|cgi|bat|pl|dll|exe|asp)$/i';
$config['system.performance']['fast_404']['html'] = '<!DOCTYPE html><html><head><title>404 Not Found</title></head><body><h1>Not Found</h1><p>The requested URL "@path" was not found on this server.</p></body></html>';

// The default list of directories that will be ignored by Drupal's file API.
$settings['file_scan_ignore_directories'] = [
  'node_modules',
  'bower_components',
];

// Private file path.
$settings['file_private_path'] = 'sites/default/files/private';

// 301 Redirects
require_once __DIR__ . '/settings.301redirects.php';
