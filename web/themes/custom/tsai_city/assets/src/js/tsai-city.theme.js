// Require the stlye files inside the "src/scss" folder.
require('src:styles/tsai-city.theme.scss');

// Require all static images inside the "src/images" folder.
require.context('src:images', true, /\.(gif|png|jpe?g|svg)$/);

require('slick-carousel/slick/slick.js');

/*WAYPOINTS*/

require('animejs');
require('floating-form-labels');


require('./partials/cas-login');
require('./partials/floating-form-labels');
require('./partials/site-nav');
require('./partials/view-slider');
require('./partials/callout-words');
