(($, window, document, undefined) => {
  $(document).ready(() => {
    if ($('a.cas-login-link').length > 0) {
      // Wrap the existing form elements in a column div.
      $('#user-login-form > *:not(.cas-login-link)').wrapAll('<div class="form__login-column">');
      $('#block-tsai-city-local-tasks').prependTo('div.form__login-column');

      // Wrap the cas link in a column div.
      $('#user-login-form .cas-login-link').wrapAll('<div class="form__cas-column">');

      // Append the login note.
      $('a.cas-login-link').after('<div class="form__cas-login-note"><strong>NOTE:</strong> Use the link above to login via NetID <strong>OR</strong> login with site specific credentials using the form to the right. Do NOT enter your NetID in this login form.</div>');

      // Prepend icon to link.
      $('a.cas-login-link').prepend('<img src="/themes/custom/tsai_city/assets/dist/images/logo-netid.svg"/>');
    }
  });
})(jQuery, window, document);
