(($, window, document, undefined) => {
  $(document).ready(() => {
    let viewSlider = document.querySelector('[data-view-slider]')

    if (viewSlider !== null) {
      let viewRows = viewSlider.querySelector('.view__rows');
      let $viewRows = $(viewRows);

      // Initialize slick.
      $viewRows.slick({
        accessibility: true,
        adaptiveHeight: false,
        arrows: true,
        autoplay: true,
        autoplaySpeed: 9000,
        dots: false,
        infinite: true,
        nextArrow: $(viewSlider.querySelector('.view__slider-next-button button')),
        pauseOnHover: true,
        prevArrow: $(viewSlider.querySelector('.view__slider-prev-button button')),
        rows: 0,
        speed: 500
      });
    }
  });
})(jQuery, window, document);
