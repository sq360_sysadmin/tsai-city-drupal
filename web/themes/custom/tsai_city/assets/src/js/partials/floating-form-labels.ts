Drupal.behaviors.floatingFormLabels = {
  attach: function(context, settings) {
    const $ = jQuery;
    const $FORM = $(context).find('[data-form]');

    if ($FORM) {
      $FORM.each(function(key, value) {
        const $FORM_ELEMENT = $(this).find('[data-form-element-floating-label]');

        $FORM_ELEMENT.floatingFormLabels({
          label: '.form__label',
          floatedClass: 'form__element--floated'
        });
      });
    }
  }
};
