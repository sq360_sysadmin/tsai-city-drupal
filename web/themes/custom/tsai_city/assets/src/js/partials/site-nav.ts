(($, window, document, undefined) => {
  $(document).ready(() => {
    const BODY = document.querySelector('body');

    /**
     * **************************************************
     * ALL CODE FOR HANDLING SITE NAVIGATION (OPEN / CLOSE)
     */

    const SITE_NAV_TOGGLE = document.querySelector('[data-site-nav-toggle]');
    const SITE_NAV = document.querySelector('[data-site-nav]');
    const SITE_NAV_WRAPPER = SITE_NAV.querySelector('[data-site-nav-wrapper]');

    let lastFocusedElementBeforeExpand;
    let focusableElements = [
      SITE_NAV_TOGGLE,
      ...SITE_NAV.querySelectorAll('a'),
    ];

    let firstFocusableElement = focusableElements[0];
    let lastFocusableElement = focusableElements[focusableElements.length - 1];

    SITE_NAV_TOGGLE.addEventListener('click', handleSiteNavToggle, false);

    function handleSiteNavToggle(event) {
      // Navigation is opened, close it.
      if (SITE_NAV_TOGGLE.getAttribute('aria-expanded') == 'true') {
        // Let screen readers know the button is not expanded.
        SITE_NAV_TOGGLE.setAttribute('aria-expanded', 'false');

        // Hide the site nav from the user and screen readers.
        SITE_NAV.setAttribute('aria-hidden', 'true');

        // Removes "keybdown" event listener.
        document.removeEventListener('keydown', handleSiteNavKeyDown);

        // Hide the site nav.
        SITE_NAV.setAttribute('style', 'visibility: hidden;');

        setTimeout(() => {
          // If there was a focused element before the .site-nav expanded,
          // return the user to that element.
          if (lastFocusedElementBeforeExpand) {
            lastFocusedElementBeforeExpand.focus();
          }
        }, 250);
      }
      // Navigation is closed, open it.
      else {
        // Let screen readers know the button is expanded.
        SITE_NAV_TOGGLE.setAttribute('aria-expanded', 'true');

        // Show the site nav from the user and screen readers.
        SITE_NAV.setAttribute('aria-hidden', 'false');
        SITE_NAV.setAttribute('style', 'visibility: visible;');

        // $(SITE_NAV_WRAPPER).niceScroll({
        //   cursorcolor: 'transparent',
        //   cursorborder: 'transparent',
        //   autohidemode: 'hidden',
        //   emulatetouch: true,
        //   grabcursorenabled: false
        // });

        // Store the last focused element before the "main-nav" took focus.
        // This element that will regain focus when the "main-nav" is collapsed.
        lastFocusedElementBeforeExpand = document.activeElement;

        // Adds "keybdown" event listener, for keyboard users.
        document.addEventListener('keydown', handleSiteNavKeyDown);

        setTimeout(() => {
          firstFocusableElement.focus();
        }, 500);
      }
    }

    /**
     * Listen for key events.
     * Valid keys are; tab, shift+tab and esc.
     *
     * @param {object} event
     */
    function handleSiteNavKeyDown(event) {
      const KEY_TAB = 9;
      const KEY_ESC = 27;

      switch(event.keyCode) {
        case KEY_TAB:
          if (focusableElements.length === 1) {
            event.preventDefault();
            break;
          }

          // Tabbing backwards
          if (event.shiftKey) {
            if (document.activeElement === firstFocusableElement) {
              event.preventDefault();
              lastFocusableElement.focus();
            }
          }
          // Tabbing forwards
          else {
            if (document.activeElement === lastFocusableElement) {
              event.preventDefault();
              firstFocusableElement.focus();
            }
          }
          break;

        case KEY_ESC:
          handleSiteNavToggle(event);
          break;

        default:
          break;
      }
    }
  });
})(jQuery, window, document);
