import anime from "animejs";

(($, window, document, undefined) => {
  $(document).ready(() => {
    const CALLOUT_WORDS = document.querySelector('[data-callout-words]');
    const PLAY_BUTTON = document.querySelector('[data-callout-play-btn]');
    const PAUSE_BUTTON = document.querySelector('[data-callout-pause-btn]');

    let calloutWordsAnime;

    if (CALLOUT_WORDS != null) {
      const CALLOUT_WORD_LETTERS = CALLOUT_WORDS.querySelector('[class$="letters"]');
      const CALLOUT_WORD_LINE = CALLOUT_WORDS.querySelector('[class$="line"]');
      const CALLOUT_WORDS_ARRAY = CALLOUT_WORDS.dataset.calloutWords.split(',');

      let calloutWordIndex = 0;

      function eachletter() {
        CALLOUT_WORD_LETTERS.innerHTML = CALLOUT_WORDS_ARRAY[calloutWordIndex];
        CALLOUT_WORD_LETTERS.innerHTML = CALLOUT_WORD_LETTERS.textContent.replace(/\S/g, '<span class="callout-word__letter">$&</span>');

        CALLOUT_WORD_LETTERS.setAttribute('aria-label', CALLOUT_WORDS_ARRAY[calloutWordIndex]);

        do_animate();
      }

      eachletter();

      function do_animate() {
        calloutWordsAnime = anime.timeline({
          loop: false
        })
        .add({
          targets: CALLOUT_WORD_LINE,
          scaleX: [0, 1],
          opacity: [0.5, 1],
          easing: "easeInOutExpo",
          duration: 900,
        })
        .add({
          targets: [...CALLOUT_WORD_LETTERS.querySelectorAll('[class$="letter"]')],
          opacity: [0, 1],
          translateX: [40, 0],
          translateZ: 0,
          scaleX: [0.3, 1],
          easing: "easeOutExpo",
          duration: 1500,
          offset: '-=600',
          delay: (el, i) => 150 + 25 * i,
        })
        .add({
          targets: [
            ...CALLOUT_WORD_LETTERS.querySelectorAll('[class$="letter"]'),
            CALLOUT_WORD_LINE
          ],
          opacity: 0,
          duration: 1000,
          easing: "easeOutExpo",
          delay: 5000,
          complete: function(anim) {
            calloutWordIndex ++;

            if (calloutWordIndex >= CALLOUT_WORDS_ARRAY.length) {
            calloutWordIndex = 0;
            }

            eachletter();
          }
        });

        PLAY_BUTTON.addEventListener('click', () => {
          playPause('play')
        }, false);


        PAUSE_BUTTON.addEventListener('click', () => {
          playPause('pause');
        }, false);
      }

      function playPause(action) {
        if (action == 'play') {
          PLAY_BUTTON.setAttribute('disabled', 'true');
          PAUSE_BUTTON.removeAttribute('disabled');

          calloutWordsAnime.play();
        }
        else if (action == 'pause') {
          PLAY_BUTTON.removeAttribute('disabled');
          PAUSE_BUTTON.setAttribute('disabled', 'true');

          calloutWordsAnime.pause();
        }
      }
    }
  });
})(jQuery, window, document);
