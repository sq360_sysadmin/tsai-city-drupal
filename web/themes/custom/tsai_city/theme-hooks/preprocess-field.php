<?php

/**
 * @file
 * preprocess-field.php
 *
 * Define all field preprocess HOOKs.
 */

use Drupal\Core\Render\Markup;

/**
 * Implements hook_preprocess_field__field_tax_time_commitments().
 */
function tsai_city_preprocess_field__field_tax_time_commitments(&$vars) {
  $commitment_map = [
    'low' => 1,
    'medium' => 2,
    'high' => 3,
  ];

  $items = $vars['items'];

  $first_item = reset($items);
  $tid = $first_item['content']['#plain_text'];

  $taxonomy_term = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($tid);
  $taxonomy_description = $taxonomy_term->description->value;
  $taxonomy_name = strtolower($taxonomy_term->name->value);

  $vars['commitment_level_count'] = $commitment_map[$taxonomy_name];
  $vars['attributes']['class'] = [];
  $vars['attributes']['class'][] = 'time-commitment';
  $vars['attributes']['class'][] = 'time-commitment--' . $taxonomy_name;
  $vars['attributes']['title'] = preg_split('/\n|\r\n?/', strip_tags($taxonomy_description));
}

/**
 * Implements hook_preprocess_field__daterange().
 */
function tsai_city_preprocess_field__daterange(&$vars) {
  foreach ($vars['items'] as &$item) {
    // Epoch timestamp.
    $start_timestamp = strtotime($item['content']['start_date']['#markup']);
    $end_timestamp = strtotime($item['content']['end_date']['#markup']);

    // Get the separate date and time parts for both start and end timestamps.
    $start_date = date('Y-m-d', $start_timestamp);
    $start_time = date('H:i:s', $start_timestamp);
    $end_date = date('Y-m-d', $end_timestamp);
    $end_time = date('H:i:s', $end_timestamp);

    // Create machine readable datetime stamps for both start and end.
    $start_machine_datetime = $start_date . 'T' . $start_time;
    $end_machine_datetime = $end_date . 'T' . $end_time;

    // When both start and end dates are the same, only print the date once
    // followed by the start - end times.
    if ($start_date == $end_date) {
      $start_date_diff = new DateTime($start_machine_datetime);
      $end_date_diff = $start_date_diff->diff(new DateTime($end_machine_datetime));

      $datetime_replacments = [
        '{$start_date_display}' => date('l, F jS, Y', $start_timestamp),
        '{$start_time_display}' => date('g:i A', $start_timestamp),
        '{$end_time_display}' => date('g:i A', $end_timestamp),
        '{$datetime}' => 'PT' . $end_date_diff->h . 'H' . $end_date_diff->i . 'M' . $end_date_diff->s . 'S',
      ];

      $daterange_output = '<time class="timestamp" datetime="{$datetime}">';
      $daterange_output .= '{$start_date_display}';
      $daterange_output .= '<span class="timestamp__time"><br>{$start_time_display} &mdash; {$end_time_display}</span>';
      $daterange_output .= '</time>';
    }
    // Otherwise, print the start and end date with the times.
    else {
      $datetime_replacments = [
        '{$start_datetime}' => $start_machine_datetime,
        '{$start_display}' => date('l, F jS, Y, g:i A', $start_timestamp),
        '{$end_datetime}' => $end_machine_datetime,
        '{$end_display}' => date('l, F jS, Y, g:i A', $end_timestamp),
      ];

      $daterange_output = '<time class="timestamp" datetime="{$start_datetime}">{$start_display}</time>';
      $daterange_output .= '<span class="timestamp__spacer"> &mdash; </span>';
      $daterange_output .= '<span class="timestamp__time"><time datetime="{$end_datetime}">{$end_display}</time></span>';
    }

    $item['content'] = Markup::create(strtr($daterange_output, $datetime_replacments));
  }
}

/**
 * Implements hook_preprocess_field__datetime().
 */
function tsai_city_preprocess_field__datetime(&$vars) {
  $field_name = $vars['field_name'];

  foreach ($vars['items'] as &$item) {
    // Epoch timestamp.
    $timestamp = strtotime($item['content']['#markup']);

    $date = date('Y-m-d', $timestamp);
    $time = date('H:i:s', $timestamp);

    // Create a machine readable datetime stamp.
    if ($time !== '00:00:00') {
      $machine_datetime = $date . 'T' . $time;
    }
    else {
      $machine_datetime = $date;
    }

    if ($field_name == 'field_dtm_published_on') {
      // Create a replacement map for start and end date/times.
      $datetime_replacments = [
        '{$date_display}' => date('F d, Y', $timestamp),
        '{$time_display}' => '',
        '{$datetime}' => $machine_datetime,
      ];
    }

    $datetime_output = '<time class="timestamp" datetime="{$datetime}">';
    $datetime_output .= '{$date_display}';

    // Include the time if it's not empty.
    if ($time !== '00:00:00') {
      $datetime_output .= ' <span class="timestamp__time">{$time_display}</span>';
    }
    $datetime_output .= '</time>';

    $item['content'] = Markup::create(strtr($datetime_output, $datetime_replacments));
  }
}

/**
 * Implements hook_preprocess_field__field_dbl_cta_link_m().
 */
function tsai_city_preprocess_field__field_dbl_cta_link_m(&$vars) {
  foreach ($vars['items'] as &$item) {
    $item_value = $item['content']['#item']->getValue();

    $original_link_text = $item_value['first'];

    $original_link_text_parts = explode(PHP_EOL, $original_link_text);

    $link_text = implode('</span><br><span>', $original_link_text_parts);
    $link_text = str_replace('\r', '', '<span>' . $link_text . '</span>');

    $item['link_text'] = $link_text;
    $item['url'] = $item_value['second'];
  }
}
