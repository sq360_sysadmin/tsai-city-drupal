<?php

/**
 * @file
 * theme.php
 *
 * Defines all custom theme templates.
 */

use Drupal\Component\Utility\Html;

/**
 * Implements hook_theme().
 */
function tsai_city_theme($existing, $type, $theme, $path) {

}

/**
 * Gets the color combo associated with the $entity.
 *
 * @param object $entity
 *   The entity, node, paragraph or taxonomy.
 *
 * @return string
 *   The color combo class.
 */
function _tsai_city_get_color_combo_class($entity) {
  if ($entity != NULL) {
    // Make sure the 'color combo' field exists.
    if ($entity->hasField('field_list_color_combo')) {
      if ($entity->get('field_list_color_combo')->count()) {
        $color_combo = $entity->get('field_list_color_combo')->first()->getValue()['value'];

        return Html::getClass($color_combo);
      }
    }
  }

  if ($entity == NULL || $entity->getEntityTypeId() != 'paragraph') {
    // Default color-combo class.
    return 'color-combo-9';
  }
}

/**
 * Gets the color combo raw value associated with the $entity.
 *
 * @param object $entity
 *   The entity, node or taxonomy.
 *
 * @return string
 *   The color combo raw hex values.
 */
function _tsai_city_get_color_combo_hex($entity) {
  $color_combo = _tsai_city_get_color_combo_class($entity);

  $color_pink = 'FF377D';
  $color_teal = '5BE1C2';
  $color_dark_blue = '1152AE';
  $color_orange = 'FF9614';
  $color_yellow = 'FFC300';
  $color_dark_green = '006346';
  $color_light_blue = '80D7F4';
  $color_purple = '6F37B0';

  $color_combo_map = [
    'color-combo-1' => $color_pink . ',' . $color_light_blue,
    'color-combo-2' => $color_dark_green . ',' . $color_light_blue,
    'color-combo-3' => $color_purple . ',' . $color_teal,
    'color-combo-4' => $color_pink . ',' . $color_teal,
    'color-combo-5' => $color_purple . ',' . $color_orange,
    'color-combo-6' => $color_dark_green . ',' . $color_orange,
    'color-combo-7' => $color_pink . ',' . $color_yellow,
    'color-combo-8' => $color_dark_blue . ',' . $color_yellow,
    'color-combo-9' => $color_dark_blue . ',' . $color_teal,
    'color-combo-10' => $color_dark_green . ',' . $color_orange,
    'color-combo-11' => $color_purple . ',' . $color_orange,
    'color-combo-12' => $color_pink . ',' . $color_yellow,
    'color-combo-13' => $color_dark_green . ',' . $color_yellow,
    'color-combo-14' => $color_dark_blue . ',' . $color_orange,
    'color-combo-15' => $color_dark_green . ',' . $color_yellow,
    'color-combo-16' => $color_purple . ',' . $color_teal,
    'color-combo-17' => $color_dark_green . ',' . $color_light_blue,
    'color-combo-18' => $color_purple . ',' . $color_light_blue,
    'color-combo-19' => $color_dark_blue . ',' . $color_teal,
    'color-combo-20' => $color_dark_blue . ',' . $color_yellow,
  ];

  return $color_combo_map[$color_combo];
}

/**
 * Gets the halftone level value associated with the $entity.
 *
 * @param object $entity
 *   The entity, node or taxonomy.
 *
 * @return string
 *   The halftone level value.
 */
function _tsai_city_get_halftone_level($entity) {
  // Make sure the 'color combo' field exists.
  if ($entity->hasField('field_list_halftone_level')) {
    if ($entity->get('field_list_halftone_level')->count()) {
      $halftone_level = $entity->get('field_list_halftone_level')->first()->getValue()['value'];

      return $halftone_level;
    }
  }
}
