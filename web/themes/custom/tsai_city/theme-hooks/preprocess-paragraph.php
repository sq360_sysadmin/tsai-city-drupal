<?php

/**
 * @file
 * preprocess-paragraph.php
 *
 * Define all paragraph preprocess HOOKs.
 */

use Drupal\Component\Utility\Html;

/**
 * Implements hook_preprocess_paragraph().
 */
function tsai_city_preprocess_paragraph(&$vars) {
  if (isset($vars['paragraph'])) {
    /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
    $paragraph = $vars['paragraph'];
    $paragraph_bundle = $paragraph->bundle();
    $paragraph_id = $paragraph->id();

    $vars['attributes']['class'] = [];
    $vars['attributes']['class'][] = _tsai_city_get_color_combo_class($paragraph);

    $vars['attributes']['data-paragraph-' . Html::getClass($paragraph_bundle)] = '';

    $function = '_' . __FUNCTION__ . '__' . $paragraph_bundle;

    if (
      $paragraph->hasField('field_bool_edge_to_edge') &&
      $paragraph->get('field_bool_edge_to_edge')->count()
    ) {
      if ($paragraph->get('field_bool_edge_to_edge')->first()->getValue()['value']) {
        $vars['attributes']['class'][] = 'paragraph--edge-to-edge';
      }
    }

    if (function_exists($function)) {
      $function($vars);
    }
  }
}

/**
 * Implements hook_preprocess_paragraph__html_content().
 */
function _tsai_city_preprocess_paragraph__html_content(&$vars) {
  if (isset($vars['paragraph'])) {
    /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
    $paragraph = $vars['paragraph'];

    if (
      $paragraph->hasField('field_list_column_width') &&
      $paragraph->get('field_list_column_width')->count()
      ) {
      $vars['attributes']['class'][] = 'paragraph--column-width-' . $paragraph->get('field_list_column_width')->first()->getValue()['value'];
    }
  }
}

/**
 * Implements hook_preprocess_paragraph__curated_content().
 */
function _tsai_city_preprocess_paragraph__curated_content(&$vars) {
  if (isset($vars['paragraph'])) {
    /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
    $paragraph = $vars['paragraph'];
    $entity_type_manager = \Drupal::entityTypeManager();

    // Get the paragraph field values.
    $node_id = $paragraph->get('field_nod_content')->first()->getString();
    $view_mode = $paragraph->get('field_list_view_mode')->first()->getString();

    // Load the node.
    /** @var \Drupal\node\Entity\Node $node */
    $node = $entity_type_manager->getStorage('node')->load($node_id);

    // Make sure it's a valid node and we have the proper access to view it.
    if ($node && $node->access('view')) {
      $render_controller = $entity_type_manager->getViewBuilder('node');
      $vars['content']['curated_node'] = $render_controller->view($node, $view_mode);
    }

    // Force all teaser featured curated nodes to display edge to edge.
    if ($view_mode == 'teaser_featured') {
      $vars['attributes']['class'][] = 'paragraph--edge-to-edge';
    }
    else {
      $vars['attributes']['class'][] = 'paragraph--inset';
    }
  }
}

/**
 * Implements hook_preprocess_paragraph__statistic().
 */
function _tsai_city_preprocess_paragraph__statistic(&$vars) {
  if (isset($vars['paragraph'])) {
    /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
    $paragraph = $vars['paragraph'];

    if (
      $paragraph->hasField('field_list_layout') &&
      $paragraph->get('field_list_layout')->count()
    ) {
      $layout_value = $paragraph->get('field_list_layout')->first()->getValue()['value'];
      $vars['attributes']['class'][] = 'paragraph--' . $layout_value;

      // "Layout 2" puts the links into the header.
      if ($layout_value == 'layout-2') {
        $vars['content']['header_links'] = $vars['content']['field_link_m'];
      }
      // All other layouts put the links into the content.
      else {
        $vars['content']['content_links'] = $vars['content']['field_link_m'];
      }
    }
  }
}

/**
 * Implements hook_preprocess_paragraph__file_download().
 */
function _tsai_city_preprocess_paragraph__file_download(&$vars) {
  if (isset($vars['paragraph'])) {
    /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
    $paragraph = $vars['paragraph'];

    $document_field = $paragraph->get('field_med_document_m');

    if (!$document_field->isEmpty()) {
      $media_document_entity = $document_field->entity;

      $document_thumbnail_uri = $media_document_entity->thumbnail->entity->uri->value;
      $document_name = $media_document_entity->name->value;
      $document_uri = $media_document_entity->get('field_media_document')->entity->uri->value;

      $vars['document_url'] = file_create_url($document_uri);
      $vars['document_name'] = $document_name;
      $vars['document_thumbnail_uri'] = $document_thumbnail_uri;
    }

  }
}
