<?php

/**
 * @file
 * preprocess-menu.php
 *
 * Define all menu preprocess HOOKs.
 */

use Drupal\Core\Render\Markup;
use Drupal\Component\Utility\Html;

/**
 * Implements hook_preprocess_menu().
 */
function tsai_city_preprocess_menu(&$vars) {
  $ignore_menus = ['admin', 'devel'];
  $menu_name = $vars['menu_name'];

  if (!in_array($menu_name, $ignore_menus)) {
    // Clear any Drupal classes.
    $vars['attributes']['class'] = [];
  }
}

/**
 * Implements hook_preprocess_menu_local_task().
 */
function tsai_city_preprocess_menu_local_task(&$vars) {
  $vars['attributes']['class'] = [];
  $vars['attributes']['class'][] = 'menu__item';
}

/**
 * Implements hook_preprocess_menu__social().
 */
function tsai_city_preprocess_menu__social(&$vars) {
  foreach ($vars['items'] as $key => &$item) {
    $link_title = $item['title'];
    $link_aria_label = "Go to Tsai City's $link_title page";

    $item['url']->setOptions([
      'attributes' => [
        'aria-label' => $link_aria_label,
        'title' => $link_title,
      ],
    ]);

    $item['title'] = Markup::create(
      file_get_contents(drupal_get_path('theme', 'tsai_city') . '/assets/dist/images/' . Html::getClass($link_title) . '.svg')
    );
  }
}
