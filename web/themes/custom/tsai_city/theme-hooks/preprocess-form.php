<?php

/**
 * @file
 * preprocess-form.php
 *
 * Define all form preprocess HOOKs.
 */

use Drupal\Component\Utility\Html;
use Drupal\Core\Template\Attribute;

/**
 * Implements hook_preprocess_form().
 */
function tsai_city_preprocess_form(&$vars) {
  $element = $vars['element'];

  // Clear any Drupal classes.
  $vars['attributes']['class'] = [];

  $vars['attributes']['class'][] = 'form';
  $vars['attributes']['class'][] = Html::getClass('form--' . $element['#form_id']);

  $vars['attributes']['data-form'] = '';
}

/**
 * Implements hook_preprocess_webform().
 */
function tsai_city_preprocess_webform(&$vars) {
  $element = $vars['element'];

  $vars['attributes']['class'][] = 'form';
  $vars['attributes']['class'][] = Html::getClass('form--webform-' . $element['#webform_id']);

  $vars['attributes']['data-form'] = '';
}

/**
 * Implements hook_preprocess_form_element().
 */
function tsai_city_preprocess_form_element(&$vars) {
  $element = $vars['element'];

  // Clear any Drupal classes.
  $vars['attributes']['class'] = [];
  $vars['attributes']['class'][] = 'form__element';

  // Non input form element.
  if ($element['#type'] == 'webform_markup') {
    $vars['attributes']['class'][] = Html::getClass('form__element--' . $element['#webform_key']);
  }
  // Input form element.
  else {
    $vars['attributes']['class'][] = Html::getClass('form__element--' . $element['#type']);

    if ($element['#type'] !== 'select') {
      $vars['attributes']['data-form-element-floating-label'] = '';
    }

    // Create an error attribute.
    $vars['error']['attributes'] = new Attribute();
    $vars['error']['attributes']['class'] = 'form__element-error-message';
  }
}

/**
 * Implements hook_preprocess_form_element_label().
 */
function tsai_city_preprocess_form_element_label(&$vars) {
  $element = $vars['element'];

  // Clear any Drupal classes.
  $vars['attributes']['class'] = [];

  $vars['attributes']['class'][] = 'form__label';
  $vars['attributes']['class'][] = Html::getClass('form__label--' . $element['#title']);

  if (
    isset($element['#required']) &&
    $element['#required']
  ) {
    $vars['attributes']['class'][] = 'form-element__label--required';
  }
}

/**
 * Implements hook_preprocess_input().
 */
function tsai_city_preprocess_input(&$vars) {
  $element = $vars['element'];

  $vars['attributes']['class'][] = 'form__input';
  $vars['attributes']['class'][] = Html::getClass('form__input--' . $element['#type']);
}
