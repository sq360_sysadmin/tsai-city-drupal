<?php

/**
 * @file
 * preprocess.php
 *
 * Define all the preprocess HOOKs.
 */

use Drupal\Component\Utility\Html;

/**
 * Implements hook_preprocess().
 */
function tsai_city_preprocess(array &$vars) {
  $vars['is_front'] = \Drupal::service('path.matcher')->isFrontPage();
}

/**
 * Implements hook_preprocess_block().
 */
function tsai_city_preprocess_block(&$vars) {
  $elements = $vars['elements'];

  if (isset($elements['#id'])) {
    $block_name = $elements['#id'];

    // Clear any Drupal classes.
    $vars['attributes']['class'] = [];

    $vars['attributes']['class'][] = 'block-' . Html::getClass($block_name);
  }
}

/**
 * Implements hook_preprocess_region().
 */
function tsai_city_preprocess_region(&$vars) {
  $elements = $vars['elements'];

  if (isset($elements['#region'])) {
    $region = $elements['#region'];

    // Clear any Drupal classes.
    $vars['attributes']['class'] = [];

    $vars['attributes']['class'][] = 'region-' . Html::getClass($region);
  }
}

/**
 * Implements hook_preprocess_html().
 */
function tsai_city_preprocess_html(&$vars) {
  // Clear any Drupal classes.
  $vars['attributes']['class'] = [];

  // Add the new class names to the array of classes.
  $vars['attributes']['class'][] = 'page';

  if ($vars['is_front']) {
    $vars['attributes']['class'][] = Html::getClass('page--is-front');
  }

  if ($node = \Drupal::routeMatch()->getParameter('node')) {
    $vars['attributes']['class'][] = Html::getClass('page--node');
    $vars['attributes']['class'][] = Html::getClass('page--node-' . $node->bundle());

    $vars['attributes']['class'][] = _tsai_city_get_color_combo_class($node);
  }
  elseif ($taxonomy_term = \Drupal::routeMatch()->getParameter('taxonomy_term')) {
    $vars['attributes']['class'][] = Html::getClass('page--term');
    $vars['attributes']['class'][] = Html::getClass('page--term-' . $taxonomy_term->bundle());

    $vars['attributes']['class'][] = _tsai_city_get_color_combo_class($taxonomy_term);
  }
  else {
    $vars['attributes']['class'][] = _tsai_city_get_color_combo_class(NULL);
  }

}

/**
 * Implements hook_preprocess_container().
 */
function tsai_city_preprocess_container(array &$vars) {
  if (isset($vars['element']['#type'])) {
    if (
      $vars['element']['#type'] == 'actions' ||
      $vars['element']['#type'] == 'webform_actions'
    ) {
      // Clear any Drupal classes.
      $vars['attributes']['class'] = [];

      $vars['attributes']['class'][] = 'form__element';
      $vars['attributes']['class'][] = 'form__element--actions';
    }
  }
}
