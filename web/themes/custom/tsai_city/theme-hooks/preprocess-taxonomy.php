<?php

/**
 * @file
 * preprocess-taxonomy.php
 *
 * Define all taxonomy preprocess HOOKs.
 */

/**
 * Implements hook_preprocess_taxonomy_term().
 */
function tsai_city_preprocess_taxonomy_term(array &$vars) {
  $elements = $vars['elements'];

  if (isset($elements['#taxonomy_term'])) {
    $taxonomy_term = $elements['#taxonomy_term'];

    $vars['attributes']['class'][] = _tsai_city_get_color_combo_class($taxonomy_term);

    unset($vars['attributes']['about']);
  }
}

function tsai_city_preprocess_taxonomy_term__topics(array &$vars) {
  $elements = $vars['elements'];

  if (isset($elements['#taxonomy_term'])) {
    $taxonomy_term = $elements['#taxonomy_term'];
    $tid = $taxonomy_term->id();

    $query = \Drupal::entityQuery('node');
    $query->condition('type', 'project');
    $query->condition('field_tax_topics_m.entity.tid', $tid);

    $project_count = $query->count()->execute();

    $vars['content']['project_count'] = $project_count;
  }
}
