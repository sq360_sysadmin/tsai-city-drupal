<?php

/**
 * @file
 * preprocess-image.php
 *
 * Define all image preprocess HOOKs.
 */

/**
 * Implements hook_preprocess_image_style().
 */
function tsai_city_preprocess_image_style(&$vars) {
  $style_name = $vars['style_name'];

  if ($style_name == 'imgix_header_image') {
    $image = &$vars['image'];
    $image_width = $image['#width'];

    // Try to get the node entity.
    $entity = \Drupal::routeMatch()->getParameter('node');

    // If it's empty, try to get the taxonomy_term entity.
    if (empty($entity)) {
      $entity = \Drupal::routeMatch()->getParameter('taxonomy_term');
    }

    // An entity exists.
    if (!empty($entity)) {
      $filename = explode('://', $vars['uri'])[1];

      $imgIX_url = \Drupal::config('imgix')->get('url');
      $imgIX_url .= $filename;
      $imgIX_url .= '?fit=clip';
      $imgIX_url .= '&w=' . $image_width;
      $imgIX_url .= '&duotone=' . _tsai_city_get_color_combo_hex($entity);
      $imgIX_url .= '&duotone-alpha=100';
      $imgIX_url .= '&htn=' . _tsai_city_get_halftone_level($entity);

      $image['#uri'] = $imgIX_url;
    }
  }
}
