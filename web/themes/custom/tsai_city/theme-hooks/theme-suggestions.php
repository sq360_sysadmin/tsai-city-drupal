<?php

/**
 * @file
 * theme-suggestions.php
 *
 * Defines all the theme suggestion HOOKs.
 */

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function tsai_city_theme_suggestions_container_alter(array &$suggestions, array $vars) {
  if (isset($vars['element']['#type'])) {

    if (
      $vars['element']['#type'] == 'webform_actions' ||
      $vars['element']['#type'] == 'actions'
    ) {
      $suggestions[] = 'container__form_element_actions';
    }

    if ($vars['element']['#type'] == 'view') {
      $suggestions[] = 'container__' .
        $vars['element']['#type'];

      $suggestions[] = 'container__' .
        $vars['element']['#type'] . '__' .
        $vars['element']['#name'];

      $suggestions[] = 'container__' .
        $vars['element']['#type'] . '__' .
        $vars['element']['#name'] . '__' .
        $vars['element']['#display_id'];
    }
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function tsai_city_theme_suggestions_node_alter(array &$suggestions, array $variables) {

  $node = $variables["elements"]["#node"];
  $view_mode = $variables["elements"]["#view_mode"];

  if ($node->bundle() == 'page' && $view_mode == 'full') {
    $suggestions[] = 'node__page__front';
  }

}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function tsai_city_theme_suggestions_taxonomy_term_alter(array &$suggestions, array $vars) {
  $term = $vars['elements']['#taxonomy_term'];
  $term_bundle = $term->bundle();

  $view_mode = strtr($vars['elements']['#view_mode'], '.', '_');

  $suggestions = [];
  $suggestions[] = 'taxonomy_term__' . $view_mode;

  $suggestions[] = 'taxonomy_term__' . $term_bundle;
  $suggestions[] = 'taxonomy_term__' . $term_bundle . '__' . $view_mode;
  $suggestions[] = 'taxonomy_term__' . $term->id();
  $suggestions[] = 'taxonomy_term__' . $term->id() . '__' . $view_mode;
}
