<?php

/**
 * @file
 * preprocess-node.php
 *
 * Define all node preprocess HOOKs.
 */

/**
 * Implements hook_preprocess_node().
 */
function tsai_city_preprocess_node(&$vars) {
  $elements = $vars['elements'];

  if (isset($elements['#node'])) {
    /** @var $node \Drupal\node\Entity\Node */
    $node = $elements['#node'];
    $node_bundle = $node->bundle();
    $node_view_mode = $elements['#view_mode'];
    $node_id = $node->id();

    $vars['attributes']['class'][] = _tsai_city_get_color_combo_class($node);

    if ($node_view_mode == 'full') {
      if (in_array($node_bundle, [
        'event',
        'fund',
        'person',
        'program',
        'project',
        'story',
      ])) {
        $vars['attributes']['class'][] = 'node--with-aside';
      }
    }

    // Remove some attributes.
    unset($vars['attributes']['role']);
    unset($vars['attributes']['about']);

    $function = '_' . __FUNCTION__ . '__' . $node_bundle;

    if (function_exists($function)) {
      $function($vars);
    }
  }
}

/**
 * Implements hook_preprocess_node__pereson().
 */
function _tsai_city_preprocess_node__person(&$vars) {
  $elements = $vars['elements'];

  if (isset($elements['#node'])) {
    /** @var $node \Drupal\node\Entity\Node */
    $node = &$elements['#node'];

    if ($node->get('field_display_name')->count()) {
      $vars['person_display_name'] = $node->get('field_display_name')->first()->getValue()['value'];
    }
    elseif ($node->get('field_first_name')->count() && $node->get('field_last_name')->count()) {
      $vars['person_display_name'] =
        $node->get('field_first_name')->first()->getValue()['value'] . ' ' .
        $node->get('field_last_name')->first()->getValue()['value'];
    }
    else {
      $vars['person_display_name'] = $node->getTitle();
    }
  }
}

/**
 * Implements hook_preprocess_node__story().
 */
function _tsai_city_preprocess_node__story(&$vars) {
  $elements = $vars['elements'];

  if (isset($elements['#node'])) {
    /** @var $node \Drupal\node\Entity\Node */
    $node = $elements['#node'];

    if (
      $node->hasField('field_dbl_original_source') &&
      $node->get('field_dbl_original_source')->count()
    ) {
      $original_source = $node->get('field_dbl_original_source')->first()->getValue();
    }

    $vars['original_source_url'] = $original_source['second'];
  }
}

/**
 * Implements hook_preprocess_node__story().
 */
function _tsai_city_preprocess_node__event(&$vars) {
  $elements = $vars['elements'];

  if (isset($elements['#node'])) {
    /** @var $node \Drupal\node\Entity\Node */
    $node = $elements['#node'];

    $system_date_config = \Drupal::config('system.date');
    $default_timezone = $system_date_config->get('timezone')['default'];

    $event_end_date = strtotime($node->get('field_dtmrng_event')->first()->getValue()['end_value']);
    $now = new DateTime("now", new DateTimeZone($default_timezone));
    $now = strtotime($now->format('Y-m-d H:i:s'));

    if ($event_end_date >= $now) {
      $vars['show_event_link'] = TRUE;
    }
    else {
      $vars['show_event_link'] = FALSE;
    }
  }
}
