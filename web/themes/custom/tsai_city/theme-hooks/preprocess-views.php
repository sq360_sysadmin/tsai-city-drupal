<?php

/**
 * @file
 * preprocess-views.php
 *
 * Define all view preprocess HOOKs.
 */

use Drupal\Component\Utility\Html;

/**
 * Implements hook_preprocess_views_view().
 */
function tsai_city_preprocess_views_view(&$vars) {
  /** @var \Drupal\views\Entity\View $view */
  $view = $vars['view'];
  $style_plugin = $view->style_plugin;
  $view_display = $style_plugin->getPluginId();
  $current_display = $view->current_display;

  // Clear any Drupal classes.
  $vars['attributes']['class'] = [];

  if ($view_display == 'grid') {
    if (isset($style_plugin->options['columns'])) {
      $vars['attributes']['class'][] = Html::getClass('view--display-grid-columns-' . $style_plugin->options['columns']);
    }
  }

  if (strpos($current_display, 'slider')) {
    $vars['attributes']['data-view-slider'] = '';
    $vars['attributes']['class'][] = 'view--display-slider';
  }
}

/**
 * Implements hook_preprocess_views_view_grid().
 */
function tsai_city_preprocess_views_view_grid(&$vars) {
  $items = [];

  foreach ($vars['items'] as $item) {
    foreach ($item['content'] as $nested_item) {
      // Clear any Drupal classes.
      $nested_item['attributes']['class'] = [];

      $items[] = $nested_item;
    }
  }

  $vars['items'] = $items;
}

